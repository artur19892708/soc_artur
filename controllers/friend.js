var UserModel   = require('../models/UserModel');
var FriendModel = require('../models/friendModel');
var RequestModel = require('../models/RequestModel');

class Friend {
	constructor(){

	}

	async allFriends(req, res){
		if(!req.session.user){
			res.redirect('/login')
		} 
		var data = await FriendModel.allFriends(req.session.user.id)
		res.render('friends',{user:req.session.user,friends:data})
	}

	deleteFriend(req, res){
		FriendModel.delete({
			user2_id: req.session.user.id,
			user1_id: req.body.userId,
		},'friends')
		res.send('ok')
	}

	addFriend(req, res){
		FriendModel.insert({
			user1_id: req.session.user.id,
			user2_id: req.body.userId,
		},'requests')
		res.send()
	}

	async requestCount(req, res){
		let data = await FriendModel.requestCount(req.session.user.id)
		res.send(data[0])

	}

	async getRequests(req, res){
		let data = await RequestModel.getRequests(req.session.user.id)
		res.send(data)
	}

}




module.exports = new Friend()