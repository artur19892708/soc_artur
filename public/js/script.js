














$('.editPhoto').change(function(){
	$(this).parents('form').submit()
})

$('.create').on('click',()=>{
	$('.hideCreate').show()
})

$('.divSelect').on('click',()=>{
	$('.hideLog').show()
})

$('.deletePost').click(function(){
	var myPost = $(this).parents().find('.myPost').attr('data_id')
	$.ajax({
		url:'/deleteMyPost',
		type:'post',
		data:{myPost:myPost},
		success:(x)=>{
			$(this).parent().remove()
		}
	})
})
$('.like').click(function(){
	var postId = $(this).parents('.myPost').attr('data_id')

	$.ajax({
		url: '/like',
		type: 'post',
		data: {postId:postId},
		success:(r) => {
			let count = $(this).next().html().substring(1, $(this).next().html().length-1)
			if(r == '1'){
				count ++
			}
			else {
				count --
			}
			$(this).next().html(`(${count})`)
		}
	})
})
