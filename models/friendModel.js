var BaseModel = require('./BaseModel')


class FriendModel extends BaseModel{
	constructor(){
		super()
		this.table = "friends"
	}

	allFriends(id){
		return new Promise((res,rej)=>{
			var hraman =`SELECT * from users 
             where id in(select user1_id from ${this.table} where user2_id=${id}
             union SELECT user2_id from ${this.table} where user1_id=${id})`

             this.db.query(hraman,function(err, data){
				if(err) throw err
				res(data)
			})
		})
	}

	deleteFriendModel(id, usId){
		return new Promise ((res,rej)=>{
			var hraman = `delete from friends
			where (user1_id=${id}
			and user2_id=${usId})
			or (user2_id=${id}
			and user1_id=${usId})`

			this.db.query(hraman,function(err,data){
				if(err) throw err
				res(data)
	        })

		})
	}

	requestCount(id){
		return new Promise ((res,rej)=>{
			var hraman = `select count(id) as count from requests 
                          where user2_id=${id}`

			this.db.query(hraman,function(err,data){
				if(err) throw err
				res(data)
	        })

		})
	}

	
}


module.exports = new FriendModel()