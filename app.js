var express      = require('express');
var bodyParser   = require('body-parser');
var mysql        = require('mysql');
var ejs          = require('ejs');
var session      = require('express-session')
var Router       = require('router')
const fileUpload = require('express-fileupload');



var app = express()
 
app.set('view engine', "ejs")
app.use("/public",express.static("public"))


app.use(fileUpload());

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true,
  //cookie: { secure: true }
}))

var router    = require('./router');
app.use('/',router)





 
app.listen(2400)