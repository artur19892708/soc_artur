var express    = require('express')
var Main       = require('./controllers/main')
var User       = require('./controllers/user')
var Friend     = require('./controllers/friend')
var Post       = require('./controllers/post')


var router = express.Router()


router.route('/').get(User.main/*controllers i Main clasai home metodn e ashxatum localhost:/2400 grlu jamanak */)
router.route('/home').get(Post.myPost)
router.route('/about').get(Main.about)
router.route('/signup').get(User.signup)
router.route('/login').get(User.login)
router.route('/profile').get(User.profile)
router.route('/logout').get(User.logout)
router.route('/edit').get(User.edit)
router.route('/friends').get(Friend.allFriends)
router.route('/user/:id').get(User.userPage)
router.route('/posts').get(Post.posts)


// router.route('/searchFriend').post(user.searchFriend)
router.route('/deleteFriend').post(Friend.deleteFriend)
router.route('/delete').post(User.delete)
router.route('/search').post(User.search)
router.route('/addFriend').post(Friend.addFriend)
router.route('/insertFriend').post(User.insertFriend)

router.route('/loginUser').post(User.loginUser)
router.route('/insertUser').post(User.insertUser)
router.route('/editPhoto').post(User.editPhoto)
router.route('/insertPost').post(Post.insertPost)
router.route('/posts').post(Post.findPost)
router.route('/deleteMyPost').post(Post.deleteMyPost)
//router.route('/editTvyalner').post(User.editTvyalner)
router.route('/like').post(Post.like)
router.route('/requestCount').post(Friend.requestCount)
router.route('/getRequests').post(Friend.getRequests)





module.exports = router