/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100130
Source Host           : localhost:3306
Source Database       : soc_artur

Target Server Type    : MYSQL
Target Server Version : 100130
File Encoding         : 65001

Date: 2019-05-25 14:04:20
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for friends
-- ----------------------------
DROP TABLE IF EXISTS `friends`;
CREATE TABLE `friends` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user1_id` int(11) DEFAULT NULL,
  `user2_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user1_id` (`user1_id`),
  KEY `user2_id` (`user2_id`),
  CONSTRAINT `friends_ibfk_1` FOREIGN KEY (`user1_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `friends_ibfk_2` FOREIGN KEY (`user2_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of friends
-- ----------------------------
INSERT INTO `friends` VALUES ('14', '36', '29');
INSERT INTO `friends` VALUES ('15', '29', '35');
INSERT INTO `friends` VALUES ('16', '29', '34');
INSERT INTO `friends` VALUES ('25', '29', '30');
INSERT INTO `friends` VALUES ('26', '29', '39');

-- ----------------------------
-- Table structure for likes
-- ----------------------------
DROP TABLE IF EXISTS `likes`;
CREATE TABLE `likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `post_id` (`post_id`),
  KEY `likes_ibfk_1` (`user_id`),
  CONSTRAINT `likes_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `likes_ibfk_2` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of likes
-- ----------------------------
INSERT INTO `likes` VALUES ('6', '30', '3');
INSERT INTO `likes` VALUES ('8', '32', '3');
INSERT INTO `likes` VALUES ('9', '34', '3');
INSERT INTO `likes` VALUES ('10', null, null);
INSERT INTO `likes` VALUES ('12', '29', '2');
INSERT INTO `likes` VALUES ('34', '29', '3');

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `text` varchar(50) DEFAULT NULL,
  `file` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of posts
-- ----------------------------
INSERT INTO `posts` VALUES ('2', '39', 'karushi post\r\n		', '1558430935121FB_IMG_1525001484985.jpg');
INSERT INTO `posts` VALUES ('3', '29', 'fut kfu kyuk ukfguk\r\n		', '1558619273473BMW-E60-M5-V10-Performance-Technic-Inc.-Tuning-Dinan-Evolve-HRE-8.jpg');
INSERT INTO `posts` VALUES ('4', '29', '\r\n		rthgfgjh', '1558622372502BF824413_8091cb.jpg');
INSERT INTO `posts` VALUES ('5', '29', '\r\n		try6tu56', '');
INSERT INTO `posts` VALUES ('6', '29', '\r\n		hertakanutyunov', '');

-- ----------------------------
-- Table structure for requests
-- ----------------------------
DROP TABLE IF EXISTS `requests`;
CREATE TABLE `requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user1_id` int(11) DEFAULT NULL,
  `user2_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user1_id` (`user1_id`),
  KEY `user2_id` (`user2_id`),
  CONSTRAINT `requests_ibfk_1` FOREIGN KEY (`user1_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `requests_ibfk_2` FOREIGN KEY (`user2_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of requests
-- ----------------------------
INSERT INTO `requests` VALUES ('1', '29', '33');
INSERT INTO `requests` VALUES ('2', '32', '30');
INSERT INTO `requests` VALUES ('3', '30', '29');
INSERT INTO `requests` VALUES ('8', '36', '29');
INSERT INTO `requests` VALUES ('9', '34', '29');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `surname` varchar(50) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf16;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('29', 'Գագիկ', 'Սարգսյան', '25', 'pox@mail.ru', 'sha1$7e11af30$1$d11407c6b0b18c3f24de3ea447dee1915f92b3ec', '155768361657915576832988331557683225286155760050552349182418_1661111530701240_8000332231908261888_n.jpg');
INSERT INTO `users` VALUES ('30', 'aram', 'aramyan', '22', 'aram@mail.ru', 'sha1$10a65546$1$1db9ae37bc8c97e69a9cf06e702f168f05d652e7', 'defolt.png');
INSERT INTO `users` VALUES ('32', 'sergey', 'simonyan', '52', 'serg@mail.ru', 'sha1$3bab0612$1$4b0687543f565acc46194f870b068a21cfe5c57e', 'defolt.png');
INSERT INTO `users` VALUES ('33', 'simon', 'simonyan', '62', 'sim@mail.ru', 'sha1$4184395f$1$a0476f73d22544eae58462474e65d1fcd8106ab6', 'defolt.png');
INSERT INTO `users` VALUES ('34', 'gaspar', 'gasparyan', '64', 'gasp@mail.ru', 'sha1$2c7ca89f$1$8f665d9730c1048dcefc871cbd9f5e5f46474f01', 'defolt.png');
INSERT INTO `users` VALUES ('35', 'levon', 'levonyan', '42', 'lev@mail.ru', 'sha1$4087bfa4$1$12ae0e336abc95fa9e0b717aa475ec17cf461b12', 'defolt.png');
INSERT INTO `users` VALUES ('36', 'karen', 'karapetyan', '28', 'kar@mail.ru', 'sha1$8d89bbd7$1$0253ddb395ca11af35fe016e10bb7e2cfcce3981', 'defolt.png');
INSERT INTO `users` VALUES ('37', 'tigran', 'hakobyan', '37', 'tik@mail.ru', 'sha1$53516070$1$1c14fb11d44ef056ffa5109ee3fd90b54aefed23', 'defolt.png');
INSERT INTO `users` VALUES ('38', 'mari', 'muradyan', '32', 'mar@mail.ru', 'sha1$c4caef00$1$e7fcaf0e5af55302fbfc61408e637a3946b2067c', 'defolt.png');
INSERT INTO `users` VALUES ('39', 'karush', 'kirakosyan', '24', 'karush@mail.ru', 'sha1$3e3397b6$1$6c6634908597f094472466466d4f50f9bf9c8785', 'defolt.png');
