var mysql = require('mysql');


class BaseModel{
	constructor(){
		this.db = mysql.createConnection({
			host     : 'localhost',
            user     : 'root',
            password : '',
            database : 'soc_artur'
        });
        this.db.connect();
	}

	insert(obj, table=this.table){//patrasti metod vorn karox enq kanchel ogtagorcel 
		return new Promise((res,rej) =>{
			let command = `insert into ${table}(
			${Object.keys(obj).join(", ")}) 
			values('${Object.values(obj).join("', '")}')`

			this.db.query(command,function(err, data){
				if(err) throw err;
				res(data)
			})
		})
	}

	find(obj, table=this.table){
		return new Promise((res,rej)=>{
			var hraman = `select * from ${table} where `;
			for(let key in obj){
				hraman += `${key} = '${obj[key]}' and ` 		
			}
			hraman = hraman.substring(0, hraman.length-5)

			hraman += ' order by id desc'

			this.db.query(hraman,function(err,data){
				if(err) throw err;
				res(data)
			})
		})
	}

	all(table = this.table){
		return new Promise((res, rej)=>{
			var hraman = `select * from ${table}`
			this.db.query(hraman,function(err,data){
				if(err) throw err;
				res(data)
			})
		})
	}

	delete(obj, table=this.table){
		return new Promise ((res,rej)=>{
			var hraman = `delete from ${table} where `;
			for(let key in obj){
				hraman += `${key} = '${obj[key]}' and `;
			}
			hraman = hraman.substring(0, hraman.length-5)
			this.db.query(hraman,function(err,data){
				if(err) throw err
				res(data)
	        })

		})
	}

}

module.exports = BaseModel