$(document).on('input','.search',function(){
	let text = $(this).val()

	$.ajax({
		url     : '/search',
		type    : 'post',
		data    : {text:text},
		success : function(r){
			$('.searchResult').show()
			$('.searchResult').empty()
	r.forEach(function(item){
		if(item.status == -1){
            return;
 		}
		var div = $(`<div class='searchUser' user_id = '${item.id}'></div> `)
		div .append(`<span>${item.name} ${item.surname}</span>`)
		
		if(item.status == 0){
			div.append(`<button class='addFriend'>Add friend </button`)
		}
		if(item.status == 1){
			div.append(`<button class=" cancel">cancel</button>`)
		}
		if(item.status == 2){
			div.append(`<button class="yes">yes</button>`)
			div.append(`<button class="no">no</button>`)
		}
		if(item.status == 3){
			div.append(`<button class="cancel">cancel</button>`)
		}
		div.appendTo(".searchResult")
	})
			if($('.search').val() == ''){
				$('.searchUser').remove()
				$('.searchResult').hide()
			}
		}
	})


})


$(document).on('click','.addFriend',function(){
	var userId = $(this).parents('.searchUser').attr('user_id')
	$.ajax({
		url: '/addFriend',
		type:'post',
		data:{userId:userId},
		success: ()=>{
			$(this).after(`<button class='cancelRequest'>cancel</button>`)
			$(this).remove()
		}
	})
})

$(document).on('click','.cancel',function(){
	var userId = $(this).parents('.searchUser').attr('user_id')
	$.ajax({
		url: '/deleteFriend',
		type:'post',
		data:{userId:userId},
		success:(x)=>{
			$(this).parent().remove()
		}
	})
})

$(document).on('click','.yes',function(){
	var userId = $(this).parents('.searchUser').attr('user_id')
	$.ajax({
		url:'/insertFriend',
		type: 'post',
		data:{userId:userId},
		success:(x)=>{
			$(this).parent().append(`<button class="cancel">cancel</button>`)
			$(this).parent().find('.no').remove()
			$(this).parent().find('.yes').remove()

		}

	})	
})
