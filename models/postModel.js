var BaseModel = require('./BaseModel')

class PostModel extends BaseModel{
	constructor(){
		super()
		this.table = 'posts'
	}

	findPosts(id){
		return new Promise((res, rej)=>{
			var hraman = `select ${this.table}.*, count(likes.id) as likeCount, 
			              users.name, users.surname, users.photo from ${this.table}
			              left join likes on likes.post_id=${this.table}.id
			              left join users on ${this.table}.user_id=users.id 
                          where ${this.table}.user_id in(
                          select user1_id from friends where user2_id=${id}
                          union
                          select user2_id from friends where user1_id=${id}
						  union
						  select id from users where id=${id}
                         )
                         GROUP BY posts.id 
                         order by post_id desc`


			this.db.query(hraman,function(err,data){
				if(err) throw err;
				res(data)
			})
		})//arandzin functia bolor mardkanc umn havasar e posti aydiin   select * from likes id
	}
}

module.exports = new PostModel()