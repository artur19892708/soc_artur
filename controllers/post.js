var UserModel   = require('../models/UserModel');
var FriendModel = require('../models/friendModel');
var PostModel = require('../models/PostModel');

class Post{
	constructor(){

	}

	insertPost(req,res){
		var fileName = ''
		if(req.files){
			var file = req.files.postFile;
			fileName = Date.now()+file.name
			file.mv('./public/postFile/'+fileName)
		}
		PostModel.insert({
			user_id: req.session.user.id,
			text: req.body.postText,
			file: fileName,
		})
		res.redirect('/profile')
	}


   posts(req,res){
		res.render('posts',{user:req.session.user})
	}

   async myPost(req, res){
		var ardyunq =await PostModel.find({
			user_id: req.session.user.id,
		})
		res.render('home',
			{ardyunq:ardyunq,user:req.session.user})
	}



	async findPost(req, res){
		var data = await PostModel.findPosts(req.session.user.id)
		res.send(data)
	}

	deleteMyPost(req, res){
		 PostModel.delete({
			id:req.body.myPost
		})
		res.send('ok')
	}

    


	async like(req, res){
		var data = await PostModel.find({
			user_id:req.session.user.id,
			post_id:req.body.postId,
		},'likes')
		if(data.length == 0){
			PostModel.insert({
				user_id:req.session.user.id,
			    post_id:req.body.postId,
		    },'likes')
		    res.send('1')
		}
		else {
			PostModel.delete({
				user_id:req.session.user.id,
			    post_id:req.body.postId,
			},'likes')
			 res.send('0')
		}
	}


}

module.exports = new Post()