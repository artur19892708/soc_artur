var BaseModel = require('./BaseModel')

class UserModel extends BaseModel{
	constructor(){
		super()
		this.table = "users"
	}

	findUser(email){
		return new Promise((res, rej)=>{
			var command = `select * from ${this.table}
			where email='${email}'`
			this.db.query(command,function(err, data){
				if(err) throw err
				res(data)
			})
		})
	}

	editPhoto(img, id){
		return new Promise((res, rej)=>{
			var command = `update ${this.table}
			set photo='${img}'
			where id = ${id}`

			this.db.query(command,function(err, data){
				if(err) throw err
				res(data)
			})
		})
	}

	search(text){
		return new Promise((res, rej)=>{
			var command = `select * from ${this.table}
				where name like '${text}%'
				or surname like '${text}%'`

			this.db.query(command,function(err, data){
				if(err) throw err
				res(data)
			})
	    })
	}

	


}

module.exports = new UserModel()