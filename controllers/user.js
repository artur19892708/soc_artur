var UserModel    = require('../models/UserModel');
var passwordHash = require('password-hash');
var FriendModel  = require('../models/FriendModel');
var RequestModel = require('../models/RequestModel');
var PostModel    = require('../models/PostModel');


class User{
	constructor(){

	}
	main(req,res){
		if(req.session.user){
			res.redirect('profile')
		}
		else{
			res.redirect('login')
		}
		
	}
	home(req,res){
		if(!req.session.user){
			res.redirect('/login')
		} 
		res.render('home',{user:req.session.user})
	}

	signup(req, res){		
		res.render('signup')
	}

	insertUser(req, res){
		var email = req.body.email;
		if(!email .includes('@mail')){
			req.session.errorSignup = 'sxal email'
			res.render('signup', {error:'sxal email'})
		}
		else{
			req.body.photo    = 'defolt.png'
		    req.body.password = passwordHash.generate(req.body.password)
		    UserModel.insert(req.body)
		    res.redirect('/login')
		}
		
		
	}

	login(req, res){
		res.render('login',{ses: req.session})
	}


	async loginUser(req, res){
		var data = await UserModel.findUser(req.body.email)
		data = data[0]
		
		if(data){
			if(passwordHash.verify(req.body.password, data.password)){
				req.session.user = await data
				res.redirect('/profile')
			}
			else{
				req.session.err   = 'password is'
				req.session.email = req.body.email
				res.redirect('/login')
			}
		}//destroy kjnje sessioni mejic
		else{
			req.session.error = 'user dosent exist'
			res.redirect('/login')
		}
	}

	async profile(req, res){
		if(!req.session.user){
			res.redirect('/login')
		} 
		var data = await PostModel.findPosts(req.session.user.id)
		console.log(data) 
		res.render('profile',{user: req.session.user, data:data})
	}

	logout(req, res){
		req.session.destroy()
		res.redirect('/login')
	}

	editPhoto(req, res){
		var img = req.files.photo
		var imgName = Date.now()+img.name
		img.mv(`./public/img/users/${imgName}`)
		UserModel.editPhoto(imgName,req.session.user.id)
		req.session.user.photo = imgName
		res.redirect('/profile')
	}

	edit(req, res){
		if(!req.session.user){
			res.redirect('/login')
		} 
		res.render('edit', {user: req.session.user})
	}

	// editTvyalner(req, res){
	// 	var user = 
	// }
	

	delete(req, res){
		UserModel.deleteModel(req.session.user);
		res.send('ok')
	}

	async search(req, res){
		var data    = await UserModel.search(req.body.text)
		var friends = await FriendModel.allFriends(req.session.user.id)
		var request = await RequestModel.allRequest(req.session.user.id)

		//eli erku hat for fra vor status2-3 
				for(let i=0; i<data.length; i++){ 
					data[i].status = 0
					if(data[i].id == req.session.user.id){
						data[i].status = -1
					}
					for(let j=0; j<friends.length; j++){ 
						if(data[i].id == friends[j].id){
							data[i].status = 1
						}
						for(let k=0; k<request.length; k++){ //1,45,29
							if(data[i].id == request[k].user1_id){
								data[i].status = 2
							}
							
						}
					}
				}

       		res.send(data)	
	}

	async userPage(req,res){
		var data = await UserModel.find({id:req.params.id})
		res.render('userPage',{
			user: req.session.user,
			data: data[0]
		})
	}

	async insertFriend(req,res){
		var data = await FriendModel.insert({user1_id:req.session.user.id,user2_id:req.body.userId})
		res.send('qcel e')
	}

}

module.exports = new User()